import { useEffect, useState } from "react";
import Construct from "./Construct.js";
import ErrorNotification from "./ErrorNotification";
import "./App.css";
import { BrowserRouter } from "react-router-dom";

function App() {
  const [launchInfo, setLaunchInfo] = useState([]);
  const [error, setError] = useState(null);

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  useEffect(() => {
    async function getData() {
      let url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/launch-details`;
      // let url2 = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}`;
      console.log("fastapi url: ", url);
      let response = await fetch(url, {
        credentials: "include",
      });
      console.log("------- hello? -------");
      let data = await response.json();

      if (response.ok) {
        console.log("got launch data!");
        setLaunchInfo(data.launch_details);
      } else {
        console.log("drat! something happened");
        setError(data.message);
      }

      // let response2 = await fetch(url2);
      // let data2 = await response.json();
      // if (response2.ok) {
      //   console.log("got launch data!");
      //   setLaunchInfo(data2.launch_details);
      // } else {
      //   console.log("drat! something happened with the second fetch");
      //   setError(data.message);
      // }
    }
    getData();
  }, []);

  return (
    <div>
      <BrowserRouter basename={basename}>
        <ErrorNotification error={error} />
        <Construct info={launchInfo} />
      </BrowserRouter>
    </div>
  );
}

export default App;
